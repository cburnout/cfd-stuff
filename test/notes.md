Tried running simpleFoam interFoam and pizoFoam.
Unsure of why it didnt work but kluged things together from the following videos

https://www.youtube.com/watch?v=1C4Av_yCfpw
https://www.youtube.com/watch?v=ZuGnO6VGeXE
https://www.youtube.com/watch?v=qfX_bJUjdOk&t=98s

This post had what I think was the answer of having -overwrite on snappyHexMesh command

https://www.cfd-online.com/Forums/openfoam-meshing/227156-setting-up-case-using-stl-files.html


Recipe:

Run
```
blockMesh
surfaceFeatureExtract
snappyHexMesh -overwrite
simpleFoam
paraFoam
```
